package model.prisoner;

public class Prisoner {
    
    private String CF;
    private String name, surname;
    private String bornDate, bornCity, bornState;
    private String address, houseNumber, CAP;
    private int identificationNumber;
    private char sector;
    private int cell;
    private String qualification;
    
    public Prisoner(String CF, String name, String surname, String bornDate, String bornCity, String bornState, String address, String houseNumber,
            String CAP, int identificationNumber, char sector, int cell, String qualification) {
        this.setCF(CF);
        this.setName(name);
        this.setSurname(surname);
        this.setBornDate(bornDate);
        this.setBornCity(bornCity);
        this.setBornState(bornState);
        this.setAddress(address);
        this.setHouseNumber(houseNumber);
        this.setCAP(CAP);
        this.setIdentificationNumber(identificationNumber);
        this.setSector(sector);
        this.setCell(cell);
        this.setQualification(qualification);
    }

    public String getCF() {
        return CF;
    }

    public void setCF(String CF) {
        this.CF = CF;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBornDate() {
        return bornDate;
    }

    public void setBornDate(String bornDate) {
        this.bornDate = bornDate;
    }

    public String getBornCity() {
        return bornCity;
    }

    public void setBornCity(String bornCity) {
        this.bornCity = bornCity;
    }

    public String getBornState() {
        return bornState;
    }

    public void setBornState(String bornState) {
        this.bornState = bornState;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCAP() {
        return CAP;
    }

    public void setCAP(String cAP) {
        CAP = cAP;
    }

    public int getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(int identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public char getSector() {
        return sector;
    }

    public void setSector(char sector) {
        this.sector = sector;
    }

    public int getCell() {
        return cell;
    }

    public void setCell(int cell) {
        this.cell = cell;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }
    
}
