package controller.editdetention;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.event.ActionEvent;

/**
 * Sample Skeleton for 'editDetention.fxml' Controller Class
 */

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import utilities.Connector;

public class EditDetentionController {

    @FXML // fx:id="moneyLaundry"
    private CheckBox moneyLaundry; // Value injected by FXMLLoader

    @FXML // fx:id="help"
    private MenuItem help; // Value injected by FXMLLoader

    @FXML // fx:id="kidnapping"
    private CheckBox kidnapping; // Value injected by FXMLLoader

    @FXML // fx:id="iIdentifier"
    private ComboBox<String> iIdentifier; // Value injected by FXMLLoader

    @FXML // fx:id="voluntaryMurder"
    private CheckBox voluntaryMurder; // Value injected by FXMLLoader

    @FXML // fx:id="Content"
    private AnchorPane Content; // Value injected by FXMLLoader

    @FXML // fx:id="x21"
    private Color x21; // Value injected by FXMLLoader

    @FXML // fx:id="x11"
    private Font x11; // Value injected by FXMLLoader

    @FXML // fx:id="insert"
    private Button insert; // Value injected by FXMLLoader

    @FXML // fx:id="robbery"
    private CheckBox robbery; // Value injected by FXMLLoader

    @FXML // fx:id="manslaughter"
    private CheckBox manslaughter; // Value injected by FXMLLoader

    @FXML
    void insertDetention(ActionEvent event) throws SQLException {
        if(!checkfield()) {
            Alert error= new Alert(AlertType.ERROR);
            error.setContentText("Selezionare un fascicolo di detenzione!");
            error.show();
        } else if(!checkAtLeastOneSelected()) {
            Alert error= new Alert(AlertType.ERROR);
            error.setContentText("Selezionare almeno un reato!");
            error.show();
        } else {
            Connection conn = Connector.GetConnectionToDB();
            Statement stmt = conn.createStatement();
            String insertCrime="";
            if(this.moneyLaundry.isSelected()) {
                insertCrime="INSERT INTO riferimento (idReati, idFascicoliDetenzione) VALUES (" + 5 + "," + this.iIdentifier.getValue() + ")";
                stmt.executeUpdate(insertCrime);
            } if(this.voluntaryMurder.isSelected()) {
                insertCrime="INSERT INTO riferimento (idReati, idFascicoliDetenzione) VALUES (" + 4 + "," + this.iIdentifier.getValue() + ")";
                stmt.executeUpdate(insertCrime);
            } if(this.manslaughter.isSelected()) {
                insertCrime="INSERT INTO riferimento (idReati, idFascicoliDetenzione) VALUES (" + 3 + "," + this.iIdentifier.getValue() + ")";
                stmt.executeUpdate(insertCrime);
            } if(this.kidnapping.isSelected()) {
                insertCrime="INSERT INTO riferimento (idReati, idFascicoliDetenzione) VALUES (" + 2 + "," + this.iIdentifier.getValue() + ")";
                stmt.executeUpdate(insertCrime);
            } if(this.robbery.isSelected()) {
                insertCrime="INSERT INTO riferimento (idReati, idFascicoliDetenzione) VALUES (" + 1 + "," + this.iIdentifier.getValue() + ")";
                stmt.executeUpdate(insertCrime);
            }
            Alert ok=new Alert(AlertType.INFORMATION, "Reati inseriti!");
            ok.show();
            stmt.close();
            ((Node)(event.getSource())).getScene().getWindow().hide();
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws SQLException {
        List<String> ids = queryIds();
        Iterator<String> idIds = ids.iterator();
        while(idIds.hasNext()) {
            this.iIdentifier.getItems().add(idIds.next());
        }
    }
    
    private List<String> queryIds() throws SQLException{
        Connection conn = Connector.GetConnectionToDB();
        Statement stmt = conn.createStatement();
        String getIds = "SELECT id FROM fascicolidetenzione";
        ResultSet resultSetIds = stmt.executeQuery(getIds);
        List<String> ids=new ArrayList<>();
        while(resultSetIds.next()) {
            ids.add(resultSetIds.getString(1));
        }
        ids.sort((a,b) -> Integer.parseInt(a) - Integer.parseInt(b));
        return ids;
    }
    
    private boolean checkfield() {
        if(this.iIdentifier.getValue()==null) {
            return false;
        }
        return true;        
    }
    
    private boolean checkAtLeastOneSelected() {
        if(this.moneyLaundry.isSelected()||this.kidnapping.isSelected()||this.voluntaryMurder.isSelected()||
                this.robbery.isSelected()||this.manslaughter.isSelected()) {
            return true;
        }
        return false;
    }
    
//    private int countNumberOfCrimes() {
//        int cont=0;
//        if(this.moneyLaundry.isSelected()) {
//            cont++;
//        } if (this.kidnapping.isSelected()) {
//            cont++;
//        } if (this.voluntaryMurder.isSelected()) {
//            cont++;
//        } if (this.robbery.isSelected()) {
//            cont++;
//        } if (this.manslaughter.isSelected()) {
//            cont++;
//        }
//        return cont;
//    }
//    
//    private int getIdOfCrime(String crime) {
//        switch (crime) {
//        case "Furto":
//            return 1;
//        case "Sequestro di persona":
//            return 2;
//        case "Omicidio colposo":
//            return 3;
//        case "Omicidio volontario":
//            return 4;
//        case "Riciclaggio denaro":
//            return 5;
//        default:
//            return 1;
//        }
//    }
    
}

