package controller.login;

import java.sql.*;

import controller.mainwindow.MainWindowController;
import javafx.event.ActionEvent;

/**
 * Sample Skeleton for 'login.fxml' Controller Class
 */

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import utilities.Connector;
import view.mainwindow.MainWindow;

public class VerifyLogin {

    @FXML // fx:id="menu"
    private MenuItem menu; // Value injected by FXMLLoader

    @FXML // fx:id="pwd"
    private PasswordField pwd; // Value injected by FXMLLoader

    @FXML // fx:id="login"
    private Button login; // Value injected by FXMLLoader

    @FXML // fx:id="user"
    private TextField user; // Value injected by FXMLLoader

    @FXML
    void VerifyUserLogin(ActionEvent event) throws SQLException {
        String username = user.getText();
        String password = pwd.getText();
        String serverUsername, serverPassword;
        if(username.isEmpty() || password.isEmpty()) {
            new Alert(Alert.AlertType.WARNING, "Inserire username e password").showAndWait();
        } else {
            Connection conn = Connector.GetConnectionToDB();
            Statement stmt = conn.createStatement();
            String findUsernameAndPassword = "SELECT dipendenti.email, dipendenti.pswd, dipendenti.matricola FROM dipendenti WHERE dipendenti.email=\""+username+"\"";
            ResultSet foundUsernameAndPassword = stmt.executeQuery(findUsernameAndPassword);
            if(foundUsernameAndPassword.next()) {
                serverUsername = foundUsernameAndPassword.getString(1);
                serverPassword = foundUsernameAndPassword.getString(2);
                if(username.equals(serverUsername) && password.equals(serverPassword)) {
                    foundUsernameAndPassword.close();
                    String checkAdmin = "SELECT amministratori.matricola FROM amministratori JOIN dipendenti ON amministratori.matricola=dipendenti.matricola WHERE dipendenti.email=\""+username+"\"";
                    ResultSet checkedAdmin = stmt.executeQuery(checkAdmin);
                    if(checkedAdmin.next()) {
                        MainWindowController.SetPermission(true);
                    }
                    checkedAdmin.close();
                    stmt.close();
                    new MainWindow(event);
                } else {
                    new Alert(Alert.AlertType.WARNING, "Errore durante il login.\nEmail e/o password sbagliati.").showAndWait();
                }
            }
        }
    }
}