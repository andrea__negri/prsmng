package controller.visitswindow;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import controller.mainwindow.MainWindowController;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import utilities.Connector;

public class VisitsWindowController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="help"
    private MenuItem help; // Value injected by FXMLLoader

    @FXML // fx:id="x1"
    private Font x1; // Value injected by FXMLLoader

    @FXML // fx:id="x2"
    private Color x2; // Value injected by FXMLLoader

    @FXML // fx:id="container"
    private ListView<String> container; // Value injected by FXMLLoader

    @FXML // fx:id="addVisit"
    private Button addVisit; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws SQLException {
        Connection conn = Connector.GetConnectionToDB();
        Statement stmt = conn.createStatement();
        String getObjects = "SELECT dataVisita, oraInizio, oraFine, nominativoReferente FROM visite JOIN detenuti ON visite.matricola=detenuti.matricola WHERE detenuti.matricola="+MainWindowController.getIndentifier();
        //System.out.println(getObjects);
        ResultSet resultSetActivities = stmt.executeQuery(getObjects);
        List<String> objects=new ArrayList<>();
        while(resultSetActivities.next()) {
            objects.add(resultSetActivities.getString(1)+ "\t|\t"+resultSetActivities.getString(2)+"\t|\t"+resultSetActivities.getString(3)+"\t|\t"+resultSetActivities.getString(4));
        }
        this.container.setItems(FXCollections.observableArrayList(objects));

    }
}
