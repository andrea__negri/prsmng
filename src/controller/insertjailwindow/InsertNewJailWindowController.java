package controller.insertjailwindow;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Pair;
import utilities.Connector;

public class InsertNewJailWindowController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="help"
    private MenuItem help; // Value injected by FXMLLoader

    @FXML // fx:id="Content"
    private AnchorPane Content; // Value injected by FXMLLoader

    @FXML // fx:id="x1"
    private Font x1; // Value injected by FXMLLoader

    @FXML // fx:id="x2"
    private Color x2; // Value injected by FXMLLoader

    @FXML // fx:id="iFiscalCode"
    private TextField iFiscalCode; // Value injected by FXMLLoader

    @FXML // fx:id="iName"
    private TextField iName; // Value injected by FXMLLoader

    @FXML // fx:id="iBirthCity"
    private TextField iBirthCity; // Value injected by FXMLLoader

    @FXML // fx:id="iBirthCountry"
    private TextField iBirthCountry; // Value injected by FXMLLoader

    @FXML // fx:id="iSurname"
    private TextField iSurname; // Value injected by FXMLLoader

    @FXML // fx:id="iAddress"
    private TextField iAddress; // Value injected by FXMLLoader

    @FXML // fx:id="iAddressNumber"
    private TextField iAddressNumber; // Value injected by FXMLLoader

    @FXML // fx:id="iAddressCountry"
    private TextField iAddressCountry; // Value injected by FXMLLoader

    @FXML // fx:id="iPostalCode"
    private TextField iPostalCode; // Value injected by FXMLLoader

    @FXML // fx:id="x11"
    private Font x11; // Value injected by FXMLLoader

    @FXML // fx:id="x21"
    private Color x21; // Value injected by FXMLLoader

    @FXML // fx:id="iSector1"
    private TextField iSector1; // Value injected by FXMLLoader

    @FXML // fx:id="iSector"
    private ComboBox<String> iSector; // Value injected by FXMLLoader

    @FXML // fx:id="iCeil"
    private ComboBox<String> iCeil; // Value injected by FXMLLoader

    @FXML // fx:id="insert"
    private Button insert; // Value injected by FXMLLoader

    @FXML // fx:id="iBirthDate"
    private DatePicker iBirthDate; // Value injected by FXMLLoader

    @FXML // fx:id="iStudy"
    private ComboBox<String> iStudy; // Value injected by FXMLLoader

    @FXML
    void insertJail(ActionEvent event) throws SQLException {
        if(!checkfield()) {
            Alert error= new Alert(AlertType.ERROR);
            error.setContentText("Tutti i campi sono obbligatori");
            error.show();
        } else {
            Connection conn = Connector.GetConnectionToDB();
            Statement stmt = conn.createStatement();
            String insertJail = "INSERT INTO Persone (codiceFiscale, nome, cognome, dataNascita, cittàNascita,\n" + 
                    "statoNascita, indirizzo, numeroCivico, CAP) VALUES (\""+this.iFiscalCode.getText()+"\",\""+this.iName.getText()+"\",\""+this.iSurname.getText()+"\",\""+ 
                    this.iBirthDate.getValue()+"\",\""+this.iBirthCity.getText()+"\",\""+this.iBirthCountry.getText()+"\",\""+this.iAddress.getText()+"\","+this.iAddressNumber.getText()+","+this.iPostalCode.getText()+")";
            stmt.executeUpdate(insertJail);
            String id="SELECT matricola FROM detenuti ORDER BY matricola DESC LIMIT 1";
            int lastId=0;
            ResultSet lastIdOnDB = stmt.executeQuery(id);
            if(lastIdOnDB.next()) {
                lastId=Integer.valueOf(lastIdOnDB.getString(1));
            }
            lastId++;
            insertJail="INSERT INTO Detenuti (matricola, codiceFiscale, idTitoliStudio, codiceSettore,\n" + 
                    "numeroCella) VALUES ("+lastId+",\""+this.iFiscalCode.getText()+"\","+getIdOfQualification(this.iStudy.getValue())+",\""+this.iSector.getValue()+"\","+this.iCeil.getValue()+")";
            stmt.executeUpdate(insertJail);
            Alert ok=new Alert(AlertType.INFORMATION, "Detenuto Inserito!\nMatricola "+lastId);
            ok.show();
            stmt.close();
            ((Node)(event.getSource())).getScene().getWindow().hide();
        }
    }
    
    private List<Pair<Integer,String>> queryQualifies() throws SQLException{
        Connection conn = Connector.GetConnectionToDB();
        Statement stmt = conn.createStatement();
        String getQualifies = "SELECT * FROM titolistudio";
        ResultSet resultSetQualifies = stmt.executeQuery(getQualifies);
        List<Pair<Integer,String>> qualifies=new ArrayList<>();
        while(resultSetQualifies.next()) {
            qualifies.add(new Pair<Integer, String>(Integer.valueOf(resultSetQualifies.getString(1)), resultSetQualifies.getString(2)));
        }
        return qualifies;
    }
    
    private List<String> querySectors() throws SQLException{
        Connection conn = Connector.GetConnectionToDB();
        Statement stmt = conn.createStatement();
        String getSectors = "SELECT settori.codiceSettore FROM settori";
        ResultSet resultSetSectors = stmt.executeQuery(getSectors);
        List <String> sectors = new ArrayList<>();
        while(resultSetSectors.next()) {
            sectors.add(resultSetSectors.getString(1));
        }
        return sectors;
    }
    
    private List<String> queryCells(String sector) throws SQLException{
        Connection conn = Connector.GetConnectionToDB();
        Statement stmt = conn.createStatement();
        String getCells = "SELECT celle.numeroCella FROM celle WHERE celle.codiceSettore=\'"+sector+"\'";
        ResultSet resultSetCells = stmt.executeQuery(getCells);
        List <String> cells = new ArrayList<>();
        while(resultSetCells.next()) {
            cells.add(resultSetCells.getString(1));
        }
        return cells;
    }
    
    private boolean checkfield() {
        if(this.iAddress.getText()==null||this.iAddressCountry.getText()==null||this.iFiscalCode.getText()==null||
                this.iAddressNumber.getText()==null||this.iBirthCity.getText()==null||this.iBirthCountry.getText()==null||
                this.iBirthDate.getValue()==null||this.iCeil.getValue()==null||this.iPostalCode.getText()==null||
                this.iName.getText()==null||this.iSector.getValue()==null||
                this.iStudy.getValue()==null||this.iSurname.getText()==null) {
            return false;
        }
        return true;
    }
    
    private int getIdOfQualification(String qualification) {
        switch (qualification) {
        case "Diploma Scuola Elementare":
            return 1;
        case "Licenza Media":
            return 2;
        case "Diploma Scuola Superiore":
            return 3;
        case "Laurea":
            return 4;
        default:
                return 1;
        }
    }
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws SQLException {
        List<Pair<Integer,String>>qualifies=queryQualifies();
        Iterator<Pair<Integer, String>> itQualifies = qualifies.iterator();
        while(itQualifies.hasNext()) {
            this.iStudy.getItems().add(itQualifies.next().getValue());
        }
        List <String> sectors = querySectors();
        Iterator<String> itSectors = sectors.iterator();
        while (itSectors.hasNext()) {
            this.iSector.getItems().add(itSectors.next());
        }
        iSector.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
            try {
                Iterator<String> it = queryCells(newValue).iterator();
                this.iCeil.getItems().clear();
                while(it.hasNext()) {
                    this.iCeil.getItems().add(it.next());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }
}

