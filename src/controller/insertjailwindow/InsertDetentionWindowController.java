package controller.insertjailwindow;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import utilities.Connector;

public class InsertDetentionWindowController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="help"
    private MenuItem help; // Value injected by FXMLLoader

    @FXML // fx:id="Content"
    private AnchorPane Content; // Value injected by FXMLLoader

    @FXML // fx:id="iErg"
    private ComboBox<String> iErg; // Value injected by FXMLLoader

    @FXML // fx:id="IDays"
    private TextField IDays; // Value injected by FXMLLoader

    @FXML // fx:id="x11"
    private Font x11; // Value injected by FXMLLoader

    @FXML // fx:id="x21"
    private Color x21; // Value injected by FXMLLoader

    @FXML // fx:id="iIdentifier"
    private ComboBox<String> iIdentifier; // Value injected by FXMLLoader

    @FXML // fx:id="insert"
    private Button insert; // Value injected by FXMLLoader

    @FXML // fx:id="iDetentionDate"
    private DatePicker iDetentionDate; // Value injected by FXMLLoader

    @FXML // fx:id="IStatus"
    private ComboBox<String> IStatus; // Value injected by FXMLLoader

    @FXML
    void insertDetention(ActionEvent event) throws NumberFormatException, SQLException {
        String days="";
        boolean checkDay=true;
        if(!checkfield()) {
            Alert error= new Alert(AlertType.ERROR);
            error.setContentText("Tutti i campi sono obbligatori");
            error.show();
        }else if(this.iErg.getValue().equals("F")&&this.IDays.getText().equals("")) {
            Alert error= new Alert(AlertType.ERROR);
            error.setContentText("Se la detenzione non prevede l'ergastolo è richiesto il numero dei giorni di reclusione!");
            error.show();
        } else {
            if(this.iErg.getValue().equals("V")){
                days="null";
            } else if(this.iErg.getValue().equals("F")){
                try {
                    Integer.parseInt(this.IDays.getText());
                    days=this.IDays.getText();
                } catch (Exception e) {
                    Alert error= new Alert(AlertType.ERROR);
                    error.setContentText("Inserire un numero intero nei giorni di reclusione!");
                    error.show();
                    checkDay=false;
                }
            }
            if(checkDay) {
                Connection conn = Connector.GetConnectionToDB();
                Statement stmt = conn.createStatement();
                String id="SELECT id FROM FascicoliDetenzione ORDER BY id DESC LIMIT 1";
                int lastId=0;
                ResultSet lastIdOnDB = stmt.executeQuery(id);
                if(lastIdOnDB.next()) {
                    lastId=Integer.valueOf(lastIdOnDB.getString(1));
                }
                lastIdOnDB.close();
                lastId++;
                String insertJail = "INSERT INTO FascicoliDetenzione (id, dataFascicolo, ergastolo, giorniReclusione,\n" + 
                        "idStatiProcesso, matricola) VALUES ("+lastId+",\""+this.iDetentionDate.getValue()+"\",\""+this.iErg.getValue()+"\","+days+","+ 
                        getIdOfStatus(this.IStatus.getValue())+","+this.iIdentifier.getValue()+")";
                stmt.executeUpdate(insertJail);
                Alert ok=new Alert(AlertType.INFORMATION, "Fascicolo Inserito!");
                ok.show();
                stmt.close();
                ((Node)(event.getSource())).getScene().getWindow().hide();
            }
        }
    }
    
    private List<String> queryStatus() throws SQLException{
        Connection conn = Connector.GetConnectionToDB();
        Statement stmt = conn.createStatement();
        String getStatus = "SELECT stato FROM statiprocesso";
        ResultSet resultSetStatus = stmt.executeQuery(getStatus);
        List<String> status=new ArrayList<>();
        while(resultSetStatus.next()) {
            status.add(resultSetStatus.getString(1));
        }
        return status;
    }

    private List<String> queryIds() throws SQLException{
        Connection conn = Connector.GetConnectionToDB();
        Statement stmt = conn.createStatement();
        String getIds = "SELECT matricola FROM detenuti";
        ResultSet resultSetIds = stmt.executeQuery(getIds);
        List<String> ids=new ArrayList<>();
        while(resultSetIds.next()) {
            ids.add(resultSetIds.getString(1));
        }
        ids.sort((a,b) -> Integer.parseInt(a) - Integer.parseInt(b));
        return ids;
    }
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws SQLException {
        List<String> status = queryStatus();
        Iterator<String> itStatus=status.iterator();
        while(itStatus.hasNext()) {
            this.IStatus.getItems().add(itStatus.next());
        }
        List<String> ids = queryIds();
        Iterator<String> idIds = ids.iterator();
        while(idIds.hasNext()) {
            this.iIdentifier.getItems().add(idIds.next());
        }
        this.iErg.getItems().add("V");
        this.iErg.getItems().add("F");
    }
    
    private boolean checkfield() {
        if(this.iDetentionDate.getValue()==null||this.iErg.getValue()==null||this.iIdentifier.getValue()==null||
                this.IDays.getText()==null||this.IStatus.getValue()==null) {
            return false;
        }
        return true;        
    }
    
    private int getIdOfStatus(String status) {
        switch (status) {
        case "Aperto":
            return 1;
        case "Chiuso":
            return 2;
        case "In Svolgimento":
            return 3;
        default:
                return 1;
        }
    }
    
}
