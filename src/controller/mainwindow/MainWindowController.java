package controller.mainwindow;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

/**
 * Sample Skeleton for 'main.fxml' Controller Class
 */

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.prisoner.Prisoner;
import utilities.Connector;
import view.editdetention.EditDetention;
import view.insertJail.InsertDetention;
import view.insertJail.InsertJailWindow;
import view.objects.ObjectsWindow;
import view.visits.VisitsWindow;

public class MainWindowController {
    
    private static boolean isAdmin=false;
    private static int id=-1;
    private List <Prisoner> prisoners;
    private ObservableList<Integer> shortListPrisoners;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;
    
    @FXML // fx:id="postalCode"
    private TextField postalCode; // Value injected by FXMLLoader

    @FXML // fx:id="objects"
    private Button objects; // Value injected by FXMLLoader

    @FXML // fx:id="x1111"
    private Font x1111; // Value injected by FXMLLoader

    @FXML // fx:id="jailContainer"
    private ListView<Integer> container; // Value injected by FXMLLoader

    @FXML // fx:id="editActivities"
    private Button editActivities; // Value injected by FXMLLoader

    @FXML // fx:id="visits"
    private Button visits; // Value injected by FXMLLoader

    @FXML // fx:id="searchText"
    private TextField searchText; // Value injected by FXMLLoader

    @FXML // fx:id="search"
    private Button search; // Value injected by FXMLLoader

    @FXML // fx:id="detention"
    private ListView<String> detention; // Value injected by FXMLLoader

    @FXML // fx:id="surname"
    private TextField surname; // Value injected by FXMLLoader

    @FXML // fx:id="fiscalCode"
    private TextField fiscalCode; // Value injected by FXMLLoader

    @FXML // fx:id="birthCountry"
    private TextField birthCountry; // Value injected by FXMLLoader
    
    @FXML // fx:id="refresh"
    private Button refresh; // Value injected by FXMLLoader

    @FXML // fx:id="x11"
    private Font x11; // Value injected by FXMLLoader

    @FXML // fx:id="sector"
    private TextField sector; // Value injected by FXMLLoader

    @FXML // fx:id="addressCountry"
    private TextField addressCountry; // Value injected by FXMLLoader

    @FXML // fx:id="identifier"
    private TextField identifier; // Value injected by FXMLLoader

    @FXML // fx:id="address"
    private TextField address; // Value injected by FXMLLoader

    @FXML // fx:id="x2111"
    private Color x2111; // Value injected by FXMLLoader

    @FXML // fx:id="birthCity"
    private TextField birthCity; // Value injected by FXMLLoader

    @FXML // fx:id="ceil"
    private TextField ceil; // Value injected by FXMLLoader

    @FXML // fx:id="birthDate"
    private TextField birthDate; // Value injected by FXMLLoader

    @FXML // fx:id="x211"
    private Color x211; // Value injected by FXMLLoader

    @FXML // fx:id="x111"
    private Font x111; // Value injected by FXMLLoader

    @FXML // fx:id="help"
    private MenuItem help; // Value injected by FXMLLoader
    
    @FXML // fx:id="administrator"
    private Menu admin; // Value injected by FXMLLoader
    
    @FXML // fx:id="insertNewJail"
    private MenuItem insertNewJail; // Value injected by FXMLLoader

    @FXML // fx:id="qualification"
    private TextField qualification; // Value injected by FXMLLoader

    @FXML // fx:id="Content"
    private AnchorPane Content; // Value injected by FXMLLoader

    @FXML // fx:id="x21"
    private Color x21; // Value injected by FXMLLoader

    @FXML // fx:id="activities"
    private ListView<String> activities; // Value injected by FXMLLoader

    @FXML // fx:id="editMode"
    private ToggleButton editMode; // Value injected by FXMLLoader

    @FXML // fx:id="name"
    private TextField name; // Value injected by FXMLLoader

    @FXML // fx:id="addressNumber"
    private TextField addressNumber; // Value injected by FXMLLoader

    @FXML // fx:id="x1"
    private Font x1; // Value injected by FXMLLoader

    @FXML // fx:id="x2"
    private Color x2; // Value injected by FXMLLoader

    public MainWindowController() throws SQLException {
        prisoners = new ArrayList<>();
        shortListPrisoners= FXCollections.observableArrayList();
        refresh();
    }
    
    public void refresh() throws SQLException {
        shortListPrisoners.clear();
        Connection conn = Connector.GetConnectionToDB();
        Statement stmt = conn.createStatement();
        String getPrisoners = "SELECT persone.*, detenuti.matricola, detenuti.codiceSettore, detenuti.numeroCella, titolistudio.qualifica FROM persone JOIN detenuti ON persone.codiceFiscale=detenuti.codiceFiscale JOIN titolistudio ON detenuti.idTitoliStudio=titolistudio.id";
        ResultSet resultSetPrisoners = stmt.executeQuery(getPrisoners);
        prisoners.clear();
        while (resultSetPrisoners.next()) {
            prisoners.add(new Prisoner(resultSetPrisoners.getString(1), resultSetPrisoners.getString(2), resultSetPrisoners.getString(3), resultSetPrisoners.getString(4),
                    resultSetPrisoners.getString(5), resultSetPrisoners.getString(6), resultSetPrisoners.getString(7), resultSetPrisoners.getString(8), resultSetPrisoners.getString(9),
                    resultSetPrisoners.getInt(10), resultSetPrisoners.getString(11).charAt(0), resultSetPrisoners.getInt(12), resultSetPrisoners.getString(13)));
            shortListPrisoners.add(resultSetPrisoners.getInt(10));
        }
        resultSetPrisoners.close();
        stmt.close();
    }
    
    public void setListViewItems () {
        this.container.setItems(shortListPrisoners);
        this.container.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->{
                try {
                    if(newValue==null) {
                        newValue=1;
                    }
                    changePrisoner(newValue);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        });
    }
    
    public void initialize() throws SQLException {
        if(isAdmin) {
            this.admin.setVisible(true);
        }
        setListViewItems();
        firstJail();
    }
    
    public void firstJail() throws SQLException {
        changePrisoner(1);
    }
    
    public void openObjectsPopup() {
        new ObjectsWindow();
    }
    
    public void openVisitsPopup() {
        new VisitsWindow();
    }
    
    public static void SetPermission(boolean status) {
        isAdmin=status;
    }
    
    public static int getIndentifier() {
        return id;
    }
    
    private void changePrisoner(Integer identificationNumber) throws SQLException {
        Prisoner prs=prisoners.get(identificationNumber-1);
        this.name.setText(prs.getName());
        this.surname.setText(prs.getSurname());
        this.fiscalCode.setText(prs.getCF());
        this.birthDate.setText(prs.getBornDate());
        this.birthCity.setText(prs.getBornCity());
        this.birthCountry.setText(prs.getBornState());
        this.address.setText(prs.getAddress());
        this.addressNumber.setText(prs.getHouseNumber());
        this.addressCountry.setText(prs.getBornState());
        this.postalCode.setText(prs.getCAP());
        this.qualification.setText(prs.getQualification());
        this.identifier.setText(String.valueOf(prs.getIdentificationNumber()));
        id=Integer.valueOf(this.identifier.getText());
        this.sector.setText(String.valueOf(prs.getSector()));
        this.ceil.setText(String.valueOf(prs.getCell()));
        
        Connection conn = Connector.GetConnectionToDB();
        Statement stmt = conn.createStatement();
        String getActivities = "SELECT nomeMansione FROM `partecipazionilav` JOIN detenuti ON partecipazionilav.matricola=detenuti.matricola JOIN attivitàlavorative ON partecipazionilav.idAttivitàLavorative=attivitàlavorative.id JOIN responsabili ON attivitàlavorative.idResponsabili=responsabili.id WHERE detenuti.matricola="+prs.getIdentificationNumber();
        //System.out.println(getActivities);
        ResultSet resultSetActivities = stmt.executeQuery(getActivities);
        List<String> activities=new ArrayList<>();
        while(resultSetActivities.next()) {
            activities.add(resultSetActivities.getString(1));
        }
        resultSetActivities.close();
        this.activities.setItems(FXCollections.observableArrayList(activities));
        String getDetention="SELECT fascicolidetenzione.dataFascicolo, reati.nome, fascicolidetenzione.ergastolo, fascicolidetenzione.giorniReclusione, statiprocesso.stato FROM fascicolidetenzione JOIN riferimento ON fascicolidetenzione.id=riferimento.idFascicoliDetenzione JOIN reati ON riferimento.idReati=reati.id JOIN statiprocesso ON fascicolidetenzione.idStatiProcesso=statiprocesso.id JOIN detenuti ON fascicolidetenzione.matricola=detenuti.matricola WHERE detenuti.matricola="+prs.getIdentificationNumber();
        ResultSet resultSetDetention = stmt.executeQuery(getDetention);
        List<String> listdetention=new ArrayList<>();
        String giorni="", ergastolo="";
        while(resultSetDetention.next()) {
            if(resultSetDetention.getString(3).contentEquals("V")) {
                ergastolo="Ergastolo";
            }
            else
            {
                ergastolo="";
            }
            if(resultSetDetention.getString(4)==null) {
                giorni="";
            }
            else
            {
                giorni=resultSetDetention.getString(4);
            }
            listdetention.add(resultSetDetention.getString(1)+"  "+resultSetDetention.getString(2)+"  "+ergastolo+"  "+giorni+"   Stato: "+resultSetDetention.getString(5));
        }
        resultSetDetention.close();
        stmt.close();
        this.detention.setItems(FXCollections.observableArrayList(listdetention));
    }
    
    public void insertNewJail() {
        new InsertJailWindow();
    }
    
    public void insertDetention() {
        new InsertDetention();
    }
    
    public void editDetention() {
        new EditDetention();
    }
    
    @FXML
    void refreshList(ActionEvent event) throws SQLException {
        refresh();
        setListViewItems();
    }
}

