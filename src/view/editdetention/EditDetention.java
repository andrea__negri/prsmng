package view.editdetention;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class EditDetention {
    public EditDetention() {
        try {
            VBox root = (VBox)FXMLLoader.load(getClass().getResource("/scenes/editDetention.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/style/application.css").toExternalForm());
            Stage mainWindow = new Stage();
            mainWindow.setTitle("Prison Manager");
            mainWindow.setScene(scene);
            mainWindow.show();
        } catch(Exception e) {
                e.printStackTrace();
        }
    }
}
