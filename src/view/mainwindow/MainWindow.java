package view.mainwindow;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainWindow {

    public MainWindow(ActionEvent event) {
        try {
            VBox root = (VBox)FXMLLoader.load(getClass().getResource("/scenes/main.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/style/application.css").toExternalForm());
            Stage mainWindow = new Stage();
            mainWindow.setTitle("Prison Manager");
            mainWindow.setScene(scene);
            mainWindow.show();
            ((Node)(event.getSource())).getScene().getWindow().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
