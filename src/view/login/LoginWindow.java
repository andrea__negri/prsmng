package view.login;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LoginWindow {
    
    public LoginWindow(Stage primaryStage) {
        try {
            VBox root = (VBox)FXMLLoader.load(getClass().getResource("/scenes/login.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/style/application.css").toExternalForm());
            primaryStage.setTitle("Prison Manager");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception e) {
                e.printStackTrace();
        }
    }
}
