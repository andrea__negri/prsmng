package view.insertJail;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class InsertDetention {
    public InsertDetention() {
        try {
            VBox root = (VBox)FXMLLoader.load(getClass().getResource("/scenes/insertDetention.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/style/application.css").toExternalForm());
            Stage mainWindow = new Stage();
            mainWindow.setTitle("Prison Manager");
            mainWindow.setScene(scene);
            mainWindow.show();
        } catch(Exception e) {
                e.printStackTrace();
        }
    }
}
